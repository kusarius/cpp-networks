#include "CommandController.h"

std::string CommandController::trim(const std::string& s)
{
	std::string::const_iterator it = s.begin();
	while (it != s.end() && isspace(*it))
		it++;

	std::string::const_reverse_iterator rit = s.rbegin();
	while (rit.base() != it && isspace(*rit))
		rit++;

	return std::string(it, rit.base());
}

void CommandController::help()
{
	std::cout << "\nCOMMAND\t\t\tDESCRIPTION\n\n";

	std::cout << "help\t\t\tShow this help\n";
	std::cout << "about\t\t\tInformation about this program and its author\n";
	std::cout << "init\t\t\tInitialize server\n";
	std::cout << "listen\t\t\tWait for client connection\n";
	std::cout << "request <type>\t\tRequest sorting (by name/type/date) on the client's host\n";
	std::cout << "exit\t\t\tExit this program\n\n";
}

void CommandController::about()
{
	std::cout << "This program is the server part of Task 1 (variant 4)\n";
	std::cout << "Made with love by Oleksandr Rahulin for GL Basecamp C++ :)\n\n";
}

void CommandController::init()
{
	switch (networkController.initialize())
	{
	case WSAInitializationFail:
		std::cout << "Windows Socket API initialization failed!\n\n";
		break;
	case SocketInitializationFail:
		std::cout << "Server socket initialization failed!\n\n";
		break;
	case SocketBindFail:
		std::cout << "Server socket binding failed!\n\n";
		break;
	case NoError:
		std::cout << "Server successfully initialized!\n\n";
		break;
	default:
		std::cout << "WTF?\n\n";
		break;
	}
}

void CommandController::listen()
{
	std::cout << "Listening for connection...\n";

	switch (networkController.listenForClient())
	{
	case ListenFail:
		std::cout << "An error occurred while listening for connections!\n\n";
		break;
	case AcceptFail:
		std::cout << "Client acception failed!\n\n";
		break;
	case NoError:
		std::cout << "Successfully connected with " << networkController.getClientIP() << "!\n\n";
		break;
	default:
		std::cout << "WTF?\n\n";
		break;
	}
}

void CommandController::request(std::string sortingType)
{
	sortingType = trim(sortingType);
	if (sortingType != "date" && sortingType != "name" && sortingType != "type")
	{
		std::cout << "Invalid argument!\n\n";
		return;
	}

	switch (networkController.sendToClient(sortingType.c_str(), sortingType.length()))
	{
	case MessageSendFail:
		std::cout << "An error occurred while sending request to client!\n\n";
		break;
	case NoError:
		int fileCount;

		if (networkController.waitForMessage((char*)&fileCount, sizeof(int)) != NoError)
		{
			std::cout << "\nAn error occured while receiving answer from client!\n";
			std::cout << "It is possible that client socket is down. Exit.\n\n";

			return;
		}
		else 
		{
			if (fileCount == -1)
				std::cout << "There was an error while sorting!\n\n";
			else
				std::cout << "Successfully sorted " << fileCount << " files!\n\n";
		}

		break;
	default:
		std::cout << "\nWTF?\n\n";
		break;
	}
}

Command CommandController::waitForCommand()
{
	std::cout << "> ";

	Command command;
	std::string enteredLine;

	getline(std::cin, enteredLine);
	enteredLine = trim(enteredLine); // Remove whitespaces
	if (enteredLine.length() == 0)
	{
		std::cout << "Please, enter the command.\n";
		command.isEmpty = true;
		return command;
	}

	std::istringstream ss(enteredLine);
	std::string argument;

	ss >> command.name;
	while (ss >> argument)
		command.arguments.push_back(argument);

	return command;
}

void CommandController::processCommand(Command command)
{
	if (command.name == "help") help();
	else if (command.name == "exit") 
	{
		networkController.closeSocket();
		exit(0);
	}
	else if (command.name == "about") about();
	else if (command.name == "init") init();
	else if (command.name == "listen") listen();
	else if (command.name == "request")
	{
		if (command.arguments.size() == 0)
		{
			std::cout << "\nThis command needs an argument (\"name\", \"type\" or \"date\")\n\n";
			return;
		}
		
		request(command.arguments[0]);
	}
}
