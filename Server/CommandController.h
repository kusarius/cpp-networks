#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <sstream>

#include "NetworkController.h"

// Why struct? Well, just because struct has public fields by default
struct Command
{
	bool isEmpty = false;

	std::string name;
	std::vector<std::string> arguments;
};

class CommandController
{
private:

	NetworkController networkController;

	std::string trim(const std::string& s);

	void help();
	void about();
	void init();
	void listen();

	void request(std::string sortingType);

public:

	// Waits for user's input
	// Returns command and its arguments (or empty command)
	Command waitForCommand();

	void processCommand(Command command);

};
