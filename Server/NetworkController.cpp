#include "NetworkController.h"

ErrorType NetworkController::initialize()
{
#ifdef _WIN32

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		return WSAInitializationFail;

#endif

	if ((serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		return SocketInitializationFail;

	sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(PORT);

	if (bind(serverSocket, (sockaddr*)&server, sizeof(server)) < 0)
		return SocketBindFail;

	return NoError;
}

ErrorType NetworkController::listenForClient()
{
	if (listen(serverSocket, 1) < 0)
		return ListenFail;

#ifdef _WIN32
	int addrlen = sizeof(sockaddr_in);
#else
	unsigned int addrlen = sizeof(sockaddr_in);
#endif

	clientSocket = accept(serverSocket, (sockaddr*)&clientInfo, &addrlen);

	if (clientSocket < 0)
		return AcceptFail;

	return NoError;
}

char* NetworkController::getClientIP()
{
	return inet_ntoa(clientInfo.sin_addr);
}

ErrorType NetworkController::sendToClient(const char* message, unsigned len)
{
	if (send(clientSocket, message, len, 0) < 0)
		return MessageSendFail;

	return NoError;
}

ErrorType NetworkController::waitForMessage(char* message, unsigned len)
{
	int receivedMessageSize;
	if ((receivedMessageSize = recv(clientSocket, message, len, 0)) < 0)
		return ReceiveFail;

	if (receivedMessageSize < len)
		message[receivedMessageSize] = '\0';

	return NoError;
}

void NetworkController::closeSocket()
{
#ifdef _WIN32
	closesocket(serverSocket);
#else
	close(serverSocket);
#endif
}
