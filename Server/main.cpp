#include "CommandController.h"

int main()
{
	std::cout << "Enter \"help\" to show the list of available commands\n\n";
	
	CommandController commandController;

	while (true)
	{
		Command c = commandController.waitForCommand();
		commandController.processCommand(c);
	}
}
