#pragma once

#include <string>

#ifdef _WIN32

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef int SOCKET;

#endif

#define PORT 1654

enum ErrorType
{
	WSAInitializationFail, SocketInitializationFail,
	SocketBindFail, MessageSendFail, ListenFail, 
	AcceptFail, ReceiveFail, NoError
};

class NetworkController
{
private:

	SOCKET serverSocket, clientSocket;
	sockaddr_in clientInfo;

public:
	
	ErrorType initialize();

	ErrorType listenForClient();

	char* getClientIP();

	ErrorType sendToClient(const char* message, unsigned len);

	ErrorType waitForMessage(char* message, unsigned len);

	void closeSocket();

};
