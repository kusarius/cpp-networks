FLAGS = --std=c++11

.PHONY: all create_build_folder

all: create_build_folder server client

create_build_folder: 
	mkdir -p build

server: Server/CommandController.cpp Server/NetworkController.cpp Server/main.cpp
	g++ -o build/server Server/CommandController.cpp Server/NetworkController.cpp Server/main.cpp $(FLAGS)

client: Client/NetworkController.cpp Client/SortingController.cpp Client/main.cpp
	g++ -o build/client Client/NetworkController.cpp Client/SortingController.cpp Client/main.cpp $(FLAGS)
