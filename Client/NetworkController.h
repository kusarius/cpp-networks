#pragma once

#include <string>

#ifdef _WIN32

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")

#else

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef int SOCKET;

#endif

#define PORT 1654

enum ErrorType
{
	WSAInitializationFail, SocketInitializationFail,
	MessageSendFail, ConnectFail, ReceiveFail, NoError
};

class NetworkController
{
private:

	SOCKET clientSocket;
	sockaddr_in serverInfo;

public:

	ErrorType initializeAndConnect(const char* serverIP);

	ErrorType sendToServer(const char* message, unsigned len);

	ErrorType waitForMessage(char* message, unsigned len);

};
