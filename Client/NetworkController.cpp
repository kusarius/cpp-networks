#include "NetworkController.h"

ErrorType NetworkController::initializeAndConnect(const char* serverIP)
{
#ifdef _WIN32

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		return WSAInitializationFail;

#endif

	if ((clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		return SocketInitializationFail;

	sockaddr_in server;
	server.sin_addr.s_addr = inet_addr(serverIP);
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);

	if (connect(clientSocket, (sockaddr*)&server, sizeof(server)) < 0)
		return ConnectFail;

	return NoError;
}

ErrorType NetworkController::sendToServer(const char* message, unsigned len)
{
	if (send(clientSocket, message, len, 0) < 0)
		return MessageSendFail;

	return NoError;
}

ErrorType NetworkController::waitForMessage(char* message, unsigned len)
{
	int receivedMessageSize;
	if ((receivedMessageSize = recv(clientSocket, message, len, 0)) <= 0)
		return ReceiveFail;

	message[receivedMessageSize] = '\0';

	return NoError;
}
