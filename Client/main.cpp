#include <iostream>
#include <string>

#include "NetworkController.h"
#include "SortingController.h"

int main()
{
	std::cout << "This is the client part of Task 1 (variant 4)\n";
	std::cout << "Made with love by Oleksandr Rahulin for GL Basecamp C++ :)\n\n";
	std::cout << "Enter the server IP: ";
	
	std::string serverIP;
	std::cin >> serverIP;

	NetworkController networkController;
	
	if (networkController.initializeAndConnect(serverIP.c_str()) != NoError)
	{
		std::cout << "An error occurred while initializing or connecting to server!\n";
		return 0;
	}

	std::cout << "Successfully connected to server!\n";

	// Infinitely listening for requests
	while (true)
	{
		char requestMessage[5] = { 0 };

		if (networkController.waitForMessage(requestMessage, 5) != NoError)
		{
			std::cout << "\nAn error occured while receiving request from server!\n";
			std::cout << "It is possible that server is down. Exit.\n\n";

			return 0;
		}
		else 
		{
			SortingController sc;
			std::vector<FileInfo> files = sc.getSortedFiles(std::string(requestMessage));

			if (sc.getSortingResult() != OK)
			{
				int failureCode = -1;
				networkController.sendToServer((char*)&failureCode, sizeof(int));

				continue;
			}

			std::cout << "\nReceived a request to sort files by " << requestMessage << ". ";
			std::cout << "Here they are:\n";

			for (int i = 0; i < files.size(); i++)
				std::cout << (i + 1) << ". " << files[i].name << "\n";

			int fileCount = files.size();
			networkController.sendToServer((char*)&fileCount, sizeof(int));
		}
	}
}
