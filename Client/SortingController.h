#ifdef _WIN32

// On Windows, I use dirent.h port written by Toni Ronkko
// https://github.com/tronkko/dirent
#include "windirent.h"

#define stat _stat

#else

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#endif

#include <vector>
#include <utility>
#include <string>
#include <algorithm>

enum SortingResult
{
    OpeningError, StatError, ClosingError, OK
};

struct FileInfo
{
    std::string name;
    std::string type;
    unsigned modTime;
};

class SortingController
{
private:

    SortingResult sortingResult;
    std::vector<FileInfo> files;

    void getFileList(std::string dirName);

public:

    SortingResult getSortingResult();

    std::vector<FileInfo> getSortedFiles(std::string sortingType);

};