#include "SortingController.h"

SortingResult SortingController::getSortingResult()
{
    return sortingResult;
}

void SortingController::getFileList(std::string dirName)
{
    DIR* dir = opendir(dirName.c_str());

    if (!dir) 
    {
        sortingResult = OpeningError;
        return;
    }

    while (true) 
    {
        dirent* entry = readdir(dir);

        if (!entry) 
            // There are no more entries in this dirirectory
            break;

        std::string entryName = std::string(entry->d_name);
        std::string entryPath = dirName + "/" + entryName;

        if (entry->d_type != DT_DIR && entry->d_name[0] != '.')
        {
            FileInfo fi;
            fi.name = entryName;
            fi.type = entryName.substr(entryName.find_last_of(".") + 1);

            struct stat s;
            if (stat(entryPath.c_str(), &s) != 0)
            {
                sortingResult = StatError;
                return;
            }

            fi.modTime = s.st_mtime;

            files.push_back(fi);
        }

        if (entry->d_type == DT_DIR && entry->d_name[0] != '.')
            if (strcmp(entry->d_name, "..") != 0 && strcmp(entry->d_name, ".") != 0)
                // Recursively call this function for each subdirectory
                getFileList(entryPath);
    }

    if (closedir(dir)) 
    {
        sortingResult = ClosingError;
        return;
    }

    sortingResult = OK;
}

std::vector<FileInfo> SortingController::getSortedFiles(std::string sortingType)
{
    files.clear();
    getFileList(".");

    auto nameComp = [](FileInfo a, FileInfo b) { return a.name < b.name; };
    auto typeComp = [](FileInfo a, FileInfo b) { return a.type < b.type; };
    auto dateComp = [](FileInfo a, FileInfo b) { return a.modTime < b.modTime; };

    if (sortingType == "name") std::sort(files.begin(), files.end(), nameComp);
    else if (sortingType == "type") std::sort(files.begin(), files.end(), typeComp);
    else std::sort(files.begin(), files.end(), dateComp);

    return files;
}