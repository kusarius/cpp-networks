This is the client/server application for sorting files on the remote host

### Compilation:
- Windows: use the Visual Studio solution
- Linux, macOS: use 'make' command

### Testing:
1. Open "server.exe" (or just "server" in "build" folder on Linux/macOS)
2. You can view the list of available commands by entering "help" in console
3. Enter "init" to initialize the server
4. Enter "listen" to start listening for connections
5. Open "client.exe" (or just "client" in "build" folder on Linux/macOS)
6. Enter the IP address of server host
7. In server application, enter "request \<name|type|date\>" to sort files on the remote host
8. Watch the result

Created for GL BaseCamp C++